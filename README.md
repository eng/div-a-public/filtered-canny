# Filtered Canny Algorithm
 
 ## Introduction

This repository contains sample scripts used to implement the Filtered Canny algorithm on MATLAB software.

This code is based on a hybrid combination of Otsu segmentation and Canny edge detection. Contours obtained using segmentation are used to form a 2D morphological mask which is applied as a spatial filter to 2D gradient maps used to compute Canny contours.

To run the code, simply input your raw image and the chosen width of the mask and run the script provided. The code will output an image with the detected edges in logical format (1: edge, 0: no edge). The code may require certain MATLAB toolboxes (i.e., Image Processing Toolbox) which can be downloaded from the official MathWorks website. To read .im7 format files, the independent PIVMat Toolbox may be required (http://www.fast.u-psud.fr/pivmat/), alongside the ReadIMX package available upon registration on the LaVision official website (http://www.lavision.de/en/).   

Note that while this code provides a solid foundation for flame front detection, it may not be optimized for all types of data. Therefore, it is recommended that you review the code and adjust it as needed to achieve the best results for your specific use case. Parameters which may require tuning are the pre-processing settings and minor morphological operations (i.e., bwareaopen, bwmorph).

## Content

- MATLAB flame front detection script (scripts/fdetect.m) -- the version used in Chaib et al. 2023 is denoted v1.0
- Raw OH-PLIF images used for demonstration purposes (images/).
- Short MATLAB code for demonstration (demo.m)..\
*Note : MATLAB R2021a+ may be required to run some parts of the code. The script fdetect.m uses MATLAB built-in functions defined differently in older versions of MATLAB.*

## Example output
![Output of the demo.m code](/figures/demo.png)

## FAQ
**Q : I get an error message "ReadIMX not found"**.\
A : The ReadIMX package is missing or it hasn't been added to the MATLAB path. Follow the instructions in http://www.fast.u-psud.fr/pivmat/html/pivmat_install.html. \
**Q : I get an error message "Undefined function or variable 'tiledlayout'" when I try to plot the images.**\
A : tiledlayout is implemented in MATLAB R2019b+.\
**Q : I get an error message "Undefined variable "images" or class "images.internal.builtins.cannyFindLocalMaxima"" when I try to run the edge detection script.**\
A : Non-maxima suppression is done using the built-in MATLAB function "cannyFindLocalMaxima" (i.e., see source code of the built-in function edge.m). It is defined differently in older versions of MATLAB which is why this error message may come up. Updating to a newer release is recommended. 
